[TOC]



我们在克隆git项目时通常都会看到可以用以下2种方式：

```java
https:
	https://gitee.com/theskyzero/sproutnew.git
ssh:
	git@gitee.com:theskyzero/sproutnew.git
```

对于我们自己的私人仓库，是需要认证登录才可以访问克隆的。这两种区别就是前者是输入用户名密码；后者则可以使用个人私钥登录[^什么是私钥]。

所以只要我们生成一堆非对称密钥对，把公钥放在git仓库，私钥留着，设置好，访问仓库项目的时候就不用总是用用户名密码认证了。

# 1. 生成密钥对

## 1.1 git工具生成



使用git bash

```shell
# 设置个人信息
git config --system user.name theskyzero
git config --system user.emain theskyzero@163.com


ssh-keygen -t rsa 

# 默认生成在C:\Users\wenxy\.ssh
# id_rsa是私钥,id_rsa.pub是公钥
```

## 1.2 puttygen生成

使用TortoiseGit工具时，使用ssh-keygen生成的key没用，需要使用putty私钥。这里我们借助TortoiseGit自带的puttygen.exe生成即可。

```shell
# TortoiseGit\bin\puttygen.exe

# 新生成
Generate -> 鼠标来回晃几下 -> 等待key生成完毕
修改key comment -> 设置 key passphrase/私钥访问密码
save public key -> save private key -> 复制上面文本框的openssh公钥保存

# 使用已有私钥
使用load加载已有私钥
修改key comment -> 设置 key passphrase/私钥访问密码
save public key -> save private key -> 复制上面文本框的openssh公钥保存
```



# 2.配置密钥

## 2.1 配置公钥

在git仓库上设置公钥

```shell
登录Gitee，打开个人设置
设置 -> 
	安全设置 -> SSH公钥

编辑id_rsa.pub，复制字符串到【公钥】

# 这里自动加了标题，说明生成密钥对时自动加了一些个人信息在里面
```

## 2.2 配置私钥

~~IDEA没有找到设置私钥位置的地方...~~

```shell
IDEA的私钥设置在
设置 -> 版本控制 -> Subversion -> SSH 
	-> Private Key -> Path
	
这个还是感觉不对
```

### 2.2.1 git配置

```shell
# 如果是使用已有的私钥，操作如下

# 使用ssh-add增加私钥
ssh-add ~/.ssh/id_rsa	

# 如果报错Could not open a connection to your authentication agent，先执行下
ssh-agent bash
```

### 2.2.2TortoiseGit配置

```
# 进入仓库位置
# TortoiseGit设置 -> Git -> Remote/远端
# 设置URL为git/ssh协议地址，设置putty私钥即可
```

# 3. 测试

使用克隆一个私有仓库，测试通过

# 4. 备份密钥和设置

自行备份密钥对

设置可使用`settings repostory`将idea设置同步到git上



------

[^什么是私钥]: 非对称加密会有一对密钥对，使用公钥加密，私钥解密。本地保留个人私钥，提供git仓库公钥就能完成数据传输过程中的加解密和认证（如何认证的呢？一般生成个人密钥对会带上一些个人信息比如用户名、组织、邮箱这些）。