[toc]



> mysql-binlog

# 1. binlog

mysql-binlog是MySQL数据库的二进制日志，用于记录用户对数据库操作的SQL语句（(除了数据查询语句）信息。可以使用mysqlbin命令查看二进制日志的内容。

## 1.1 格式

binlog的格式也有三种：STATEMENT、ROW、MIXED 。

> 在 MySQL 5.7.7 之前，默认的格式是 STATEMENT，在 MySQL 5.7.7 及更高版本中，默认值是 ROW。日志格式通过 binlog-format 指定，如 binlog-format=STATEMENT、binlog-format=ROW、binlog-format=MIXED。

- STATMENT模式：
	- 基于SQL语句的复制(statement-based replication, SBR)，每一条会修改数据的sql语句会记录到binlog中。
- 优点：不需要记录每一条SQL语句与每行的数据变化，这样子binlog的日志也会比较少，减少了磁盘IO，提高性能。
	
- 缺点：在某些情况下会导致master-slave中的数据不一致(如sleep()函数， last_insert_id()，以及user-defined functions(udf)等会出现问题)
- 基于行的复制(row-based replication, RBR)：
	- 不记录每一条SQL语句的上下文信息，仅需记录哪条数据被修改了，修改成了什么样子了。
	- 优点：不会出现某些特定情况下的存储过程、或function、或trigger的调用和触发无法被正确复制的问题。
	- 缺点：会产生大量的日志，尤其是alter table的时候会让日志暴涨。
- 混合模式复制(mixed-based replication, MBR)：
  - 以上两种模式的混合使用，一般的复制使用STATEMENT模式保存binlog，对于STATEMENT模式无法复制的操作使用ROW模式保存binlog，MySQL会根据执行的SQL语句选择日志保存方式。

## 1.2 使用场景

- **MySQL主从复制**：MySQL Replication在Master端开启binlog，Master把它的二进制日志传递给slaves来达到master-slave数据一致的目的
- **数据恢复**：通过使用 mysqlbinlog工具来使恢复数据

## 1.3 日志格式

binlog日志包括两类文件:

二进制日志索引文件（文件名后缀为.index）用于记录所有有效的的二进制文件

二进制日志文件（文件名后缀为.00000*）记录数据库所有的DDL和DML语句事件。

> binlog是一个二进制文件集合，每个binlog文件以一个4字节的魔数开头，接着是一组Events:
>
> - 魔数：0xfe62696e对应的是0xfebin；
>
> - Event：每个Event包含header和data两个部分；
>   - header提供了Event的创建时间，哪个服务器等信息，
>   - data部分提供的是针对该Event的具体信息，如具体数据的修改；
> 
> 第一个Event用于描述binlog文件的格式版本，这个格式就是event写入binlog文件的格式；其余的Event按照第一个Event的格式版本写入；最后一个Event用于说明下一个binlog文件；
> 
> binlog的索引文件是一个文本文件，其中内容为当前的binlog文件列表

## 1.4 写 Binlog 的时机

对支持事务的引擎如InnoDB而言，必须要提交了事务才会记录binlog。binlog 什么时候刷新到磁盘跟参数 sync_binlog 相关。

如果设置为0，则表示MySQL不控制binlog的刷新，由文件系统去控制它缓存的刷新；
如果设置为不为0的值，则表示每 sync_binlog 次事务，MySQL调用文件系统的刷新操作刷新binlog到磁盘中。
设为1是最安全的，在系统故障时最多丢失一个事务的更新，但是会对性能有所影响。
如果 sync_binlog=0 或 sync_binlog大于1，当发生电源故障或操作系统崩溃时，可能有一部分已提交但其binlog未被同步到磁盘的事务会被丢失，恢复程序将无法恢复这部分事务。

当遇到以下3种情况时，MySQL会重新生成一个新的日志文件，文件序号递增：

- MySQL服务器停止或重启时

- 使用 `flush logs` 命令；

- 当 binlog 文件大小超过 `max_binlog_size` 变量的值时；

  ```
  max_binlog_size 的最小值是4096字节，最大值和默认值是 1GB (1073741824字节)。事务被写入到binlog的一个块中，所以它不会在几个二进制日志之间被拆分。因此，如果你有很大的事务，为了保证事务的完整性，不可能做切换日志的动作，只能将该事务的日志都记录到当前日志文件中，直到事务结束，你可能会看到binlog文件大于 max_binlog_size 的情况。
  ```



# 2. 配置

## 2.1 开启binlog

```shell
# 编辑mysql/my.cnf
[mysqld]
log-bin=mysql-bin # (也可指定二进制日志生成的路径，如：log-bin=/opt/Data/mysql-bin)
server-id=1
binlog_format=ROW # (加入此参数才能记录到insert语句，STATEMENT、ROW、MIXED )
expire_logs_days = 7 # binlog清理
# max_binlog_size = 100m #
# binlog_cache_size = 4m
# max_binlog_cache_size = 512m
```

```shell
# 重启mysqld
```

```mysql
# 查看是否开启binlog
mysql> show variables like 'log_bin';
```

## 2.2 查看binlog

```mysql
# 查看日志文件
show master logs;	

# 查看master状态
show master status;

# 查询binlog events
show binlog events [IN 'log_name'] [FROM pos] [LIMIT [offset,] row_count];

# 使用mysqlbinlog查看日志傻里傻气
mysqlbinlog --no-defaults mysql-bin.000720 --start-datetime="2018-09-12 18:45:00" --stop-datetime="2018-09-12:18:47:00"
```

## 2.3 操作binlog

```mysql
# 刷新binlog
flush logs;

# 清空binlog
reset master;
```

## 2.4 使用binlog

```mysql
# 手残各种瞎操作

# 先备份一下最后一个binlog日志文件：
cp -v mysql-bin.000004  /application/data/backup/

# 接着执行一次刷新日志索引操作，重新开始新的binlog日志记录文件。便于我们分析原因及查找ops节点，以后所有数据库操作都会写入到下一个日志文件
flush logs;
show master status;

# 读取binlog日志确认故障position
show binlog events in 'mysql-bin.000003';

# 找个备份库恢复备份
# 使用binlog正常点恢复
mysqlbinlog --stop-datetime="2018-09-12 10:37:58"  /application/data/backup/mysql-bin.000002 | /application/mysql3307/bin/mysql -uroot -S /application/mysql3307/logs/mysql.sock -p123456 -v
```

