[toc]



docker 安装使用 mysql



# 1. 下载镜像

```shell
# 搜索镜像
docker search mysql

# web
# 阿里云 user open
https://cr.console.aliyun.com/cn-hangzhou/instances/images 
# office
https://hub.docker.com/_/mysql 

# pull
docker pull mysql:5.7
```

# 2. 运行

## 2.1 run

```shell
# 总结
docker run --name mysql -h db-mysql -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7

docker cp mysql:/var/lib/mysql/my.cnf /home/mysql 
docker cp /home/mysql/my.cnf mysql:/var/lib/mysql/my.cnf
```



```shell
# simple server instance
docker run --name mysql-simple -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7

# command line client, network is for docker containters
docker run -it --network some-network --rm --name mysql-command -h some-mysql -u example-user -p mysql:5.7
docker run -it --rm --name mysql -h some.mysql.host -u some-mysql-user -p mysql:5.7

# configuration file
docker run --name mysql-configuration -v /home/mysql/config:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
docker run --name mysql-configuration -v /home/mysql/config/config-file.cnf:/etc/mysql/conf.d/config-file.cnf -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7

# no configuration file, // docker run -it --rm mysql:tag --verbose --help
docker run --name mysql-no-config -e MYSQL_ROOT_PASSWORD=123456 -d mysql:tag --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

# /var/lib/mysql/data
docker run --name mysql-data -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7

# Running as an arbitrary user( 不同用户权限问题)
# 查看文件夹用户权限
ls -lnd data	# drwxr-xr-x 2 1000 1000 4096 Aug 27 15:54 data
# 设置挂在文件夹权限	--user
docker run -v "$PWD/data":/var/lib/mysql --user 1000:1000 --name mysql-arbuser -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7
```

```yml
# docker-compose
# Use root/example as user/password credentials
version: '3.1'

services:

  db-mysql:
    image: mysql:5.7
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 123456

  adminer-mysql:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```

## 2.2 exec

```shell
# Creating database dumps
$ docker exec mysql-dumps sh -c 'exec mysqldump --all-databases -uroot -p"123456"' > /home/mysql/dumps/all-databases.sql

# Restoring data from dump files
$ docker exec -i some-mysql sh -c 'exec mysql -uroot -p"123456"' < /home/mysql/dumps/all-databases.sql
```

