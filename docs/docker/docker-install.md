[toc]



> docker 安装设置

# 1. windows

## 1.1 下载

[docker-desktop](https://www.docker.com/products/docker-desktop)

双击安装即可，安装完重启电脑，任务栏有个小鲸鱼

## 1.2 设置

右击小鲸鱼 -> settings

```yaml
General 
Resource:
	Advanced: 设置cpu、内存、硬盘、磁盘位置
	File Sharing: 挂载硬盘到docker
	Proxies:
	Network:
Docker Engine: (daemon.json)
	# 是用自己的阿里云镜像加速
	registry-mirrors: https://r2vk8r09.mirror.aliyuncs.com
	# 这里想设置pull镜像位置的==
Comand Line: enable
Kubernetes:
```

# 2. ubuntu



# 3. centos

好了，刚刚折腾完centos7，现在来装docker吧~

## 3.1 下载

centos7是3.10的内核，支持docker的

```shell
[wenxy@localhost ~]$ uname -a
Linux localhost.localdomain 3.10.0-1127.el7.x86_64 #1 SMP Tue Mar 31 23:36:51 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

### 3.1.1 yum update

```shell
# yum 更新， linux包管理方式真是太多了...为什么大家还觉得是优点呢？...真是不同的开发习惯啊不同的开源版本啊...
[root@localhost wenxy]# yum update
 
 # 哇靠，这点我人都傻了，调到了我的idm，疯狂请求下载文件，我换到虚拟机里try下
 # 简直傻掉了，在虚拟机里也不行...
```

### 3.1.2 安装依赖

```shell
3 yum-util 提供yum-config-manager功能，另外两个是devicemapper驱动依赖的
[root@localhost wenxy]# yum install -y yum-utils device-mapper-persistent-data lvm2
```

### 3.1.2 设置docker源

```shell
# 我猜我会用第1条吧
# （阿里仓库）
[root@localhost wenxy]# yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#（中央仓库）
yum-config-manager --add-repo http://download.docker.com/linux/centos/docker-ce.repo

yum-config-manager --disable http://download.docker.com/linux/centos/docker-ce.repo

yum-config-manager -add-repo http://download.docker.com/linux/centos/docker-ce.repo
```

### 3.1.3 查看docker版本

```shell
[root@localhost wenxy]# yum list docker-ce --showduplicates | sort -r
```

### 3.1.4 安装docker

```shell
# 可以指定版本安装
# [root@localhost ~]# yum install docker-ce-18.03.1.ce -y

# 所以我没有指定，安装了最新版
[root@localhost wenxy]# yum install docker-ce -y
```

不得不赞一句，centos安装软件，终端的界面是真好看...爱了

### 3.1.5 启动docker

```shell
# 启动
[root@localhost wenxy]# systemctl start docker

# 加入开机自启
[root@localhost wenxy]# systemctl enable docker
```

