[toc]



docker 安装使用 redis

> 注：docker默认需要docker用户组权限，添加用户至docker组即可直接使用命令

### 1. 下载镜像

```shell
# docker pull redis:tag

docker pull redis

# 其实直接run也可以，没有image会自动下载的
```

### 2. 运行

```shell
docker run --name redis redis
```



### 3. 查看

redis官方镜像极其小巧，还没有配置文件，直接在`/usr/local/bin`下提供了`redis-server`、`redis-cli`等命令，所以需要自己把`redic.conf`传上去