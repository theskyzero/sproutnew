[toc]





https://www.docker.elastic.co/#

# 1. elasticsearch



## 1.1 tag

[elasticsearch支持的tag](https://hub.docker.com/_/elasticsearch?tab=tags&page=1)

## 1.2 pull

```shell
docker pull elasticsearch

# docker pull elasticsearch:7.7.0
```

## 1.3 run

### 1.3.1 single-node cluster

```shell
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.7.0
```

### 1.3.2 multi-node cluster with a docker compose file

```yml
# docker-compose.yml
version: '2.2'
services:
  es01:
    image: elasticsearch:7.7.0
    container_name: es01
    environment:
      - node.name=es01
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es02,es03
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data01:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - elastic
  es02:
    image: elasticsearch:7.7.0
    container_name: es02
    environment:
      - node.name=es02
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es01,es03
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data02:/usr/share/elasticsearch/data
    networks:
      - elastic
  es03:
    image: elasticsearch:7.7.0
    container_name: es03
    environment:
      - node.name=es03
      - cluster.name=es-docker-cluster
      - discovery.seed_hosts=es01,es02
      - cluster.initial_master_nodes=es01,es02,es03
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data03:/usr/share/elasticsearch/data
    networks:
      - elastic

volumes:
  data01:
    driver: local
  data02:
    driver: local
  data03:
    driver: local

networks:
  elastic:
    driver: bridge
```

```shell
docker-compose up

# curl -X GET "localhost:9200/_cat/nodes?v&pretty"
# docker-compose down
```

## 1.4 settings

### 1.4.1  set `vm.max_map_count`

```shell
# get
grep vm.max_map_count /etc/sysctl.conf
# vm.max_map_count=262144

# set
sysctl -w vm.max_map_count=262144
```

### 1.4.2 set user elasticsearch

```shell
# 我们是在虚拟机的docker里的，可能还得创建elasticsearch用户才行，也不一定，说不定userid就可以

# 设置挂载目录
mkdir esdatadir
chmod g+rwx esdatadir
chgrp 1000 esdatadir
```

### 1.4.3 set nofile & nproc

```shell
docker run --ulimit nofile=65535:65535

# 登录容器执行ulimit -Hn && ulimit -Sn && ulimit -Hu && ulimit -Su
# docker run --rm centos:7 /bin/bash -c 'ulimit -Hn && ulimit -Sn && ulimit -Hu && ulimit -Su'
```

### 1.4.4 disable swap

```shell
sudo swapoff -a

# docker run -e "bootstrap.memory_lock=true" --ulimit memlock=-1:-1
```

### 1.4.5 set publish ports

```shell
# randomizing the published ports and then inspect containter
docker run --publish-all 
```

### 1.4.6 set heap size

```shell
docker run -e ES_JAVA_OPTS="-Xms16g -Xmx16g"
```

### 1.4.7 bind volumn

```
docker -v /path:/usr/share/elasticsearch/data
```

# 2. logstash

## 2.1 tag

[https://www.docker.elastic.co](https://www.docker.elastic.co)

## 2.2 pull

```shell
docker pull logstash
```

## 2.3 run

```
docker run -it -v ~/pipeline/:/usr/share/logstash/pipeline/ logstash:7.7.0
```

## 2.4 settings

### 2.4.1 bind config

```
docker run -it -v ~/settings/:/usr/share/logstash/config/ logstash:7.7.0

# docker run -it -v ~/settings/logstash.yml:/usr/share/logstash/config/logstash.yml logstash:7.7.0
```

# 3. kibana

## 3.1 tag

[https://www.docker.elastic.co](https://www.docker.elastic.co)

## 3.2 pull

```
docker pull kibana:7.7.0
```

## 3.3 run

```shell
# docker network create somenetwork
docker run -d --name kibana --net somenetwork -p 5601:5601 kibana:7.7.0

# docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 {docker-repo}:{version}
docker run --link elasticsearch:elasticsearch -p 5601:5601 kibana:7.7.0
```

## 3.4 settings

### 3.4.1 bind config

```yaml
version: '2'
services:
  kibana:
    image: kibana:7.7.0
    volumes:
      - ./kibana.yml:/usr/share/kibana/config/kibana.yml
    # environment:
      # SERVER_NAME: kibana.example.org
      # ELASTICSEARCH_HOSTS: http://elasticsearch.example.org
```