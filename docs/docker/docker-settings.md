[toc]



前面我们已经安装好了docker，因为在虚拟机里，我们希望能做一些额外的配置

1. 切换仓库源为国内源（阿里云）
2. 设置docker目录，映射到主机
3. 普通用户执行docker命令

为了更快，同时减少虚拟机的占用以及下载资源的复用。



# 1. 设置docker目录

## 1.1 挂载主机目录

挂载到 `/mnt/hgfs/F`

## 1.2 设置docker目录

这里应该和之前ubuntu是类似的，我们直接看一下

修改docker仓库保存位置，映射到主机

```shell
#指定镜像和容器存放路径的参数是--data-root=/var/lib/docker
sudo vi /usr/lib/systemd/system/docker.service 

# ExecStart=/usr/bin/dockerd 最后增加参数(映射主机的路径)
ExecStart=/usr/bin/dockerd --data-root=/mnt/hgfs/F/Resources/Tools/docker --storage-driver=devicemapper -H fd:// --containerd=/run/containerd/containerd.sock  

# 设置国内镜像加速(这里复制可能有问题，我都是在阿里云镜像服务上复制)
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://r2vk8r09.mirror.aliyuncs.com"]
}
EOF

# 重启docker服务
sudo systemctl daemon-reload
sudo systemctl restart docker

# ExecStart=/usr/bin/dockerd --storage-driver=devicemapper --storage-opt dm.thinpooldev --data-root=/mnt/hgfs/F/Resources/Tools/docker -H fd:// --containerd=/run/containerd/containerd.sock  

# 重启docker服务
sudo systemctl daemon-reload
sudo systemctl  restart docker.service
```

# 2. 普通用户执行权限

增加非root用户运行docker服务（加入docker组）

```shell
# 创建docker组（好像默认创建过了）
sudo groupadd docker

# 加入当前用户 sudo usermod -aG docker jacky
sudo gpasswd -a ${USER} docker

# 更新docker组（网上到处是退出再登录）
 newgrp docker

# 试一下
docker --help
```

