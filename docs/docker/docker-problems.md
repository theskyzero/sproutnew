[toc]



这里记录下平时遇到的问题吧…



# 1. 文件权限类

## 1.1 读写挂载的volumn没权限

### 1.1.1 案例1： mysql挂载`/var/lib/mysql`读写被拒绝导致启动失败

```
mysqld: Can't create/write to file '/var/lib/mysql/is_writable' (Errcode: 13 - Permission denied)
```

> 环境：主机win10+vm虚拟机Ubuntu20+docker+mysql:5.7
>
> 配置：主机win10共享文件夹挂载docker目录
>
> 问题分析：
>
> 1. 主机win10挂载docker文件目录，docker下载的image和镜像都实际保存在主机win10，因为vm共享文件夹的原因，挂载的目录用户和组都无法更改
>
> 2. mysql镜像使用mysql用户创建的mysql数据库，使用volumn会随机在容器下生成文件夹挂载`/var/lib/mysql`，默认会导致docker容器内组用户和挂载在虚拟机上的组用户不一致，导致权限不一致
>
>    ```
>    # 查看挂载文件夹，默认在docker/volumns/*
>    docker inspect containername  
>    ```
>
> 3. 手动挂载在虚拟机内也不行（这里不知道为什么，比较奇怪，手动挂载应该是个比较正常的操作，为什么这里会失败呢？docker不挂载在主机win10上，启动是正常的）
>
> 处理方案：
>
> 1. 确定挂载文件夹的用户和及权限
> 2. 手动挂载文件夹在虚拟机
> 3. 虚拟机内修改挂载文件夹，并赋予权限
>
> 操作步骤：
>
> ```shell
> # 查看容器中/var/lib/mysql的所有者
> # 因为容器挂了，这里用run新建一个仅查看权限-rm用来关闭容器后删除容器
> docker run -it --rm --entrypoint="/bin/bash" mysql:5.7 -c "ls -la /var/lib/mysql"
> 
> # 查看容器里的mysql用户组id:
> # 这里确认用户组id，用来下面直接用id授权（没试过之前傻乎乎的新建用户）
> docker run -ti --rm --entrypoint="/bin/bash" mysql:5.7 -c "cat /etc/group"
> 
> # 修改挂载文件用户组id（虚拟机的挂载文件）
> chown -R 999 /home/data
> 
> # 这里是不是也可以挂载主机呢？
> ```
>
> 

