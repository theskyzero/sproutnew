[toc]

#### 前言
为什么会想写个swagger的东西呢？

1. 首先虽然这确实与code关系不是那么大
2. 而且身为开发，真的是自己不想写文档写注释，却总是希望比人写。尤其是当他接手前人的烂摊子或者过一段时间收拾自己的烂摊子...
3. 恰逢其会，学习和工作中都有用到

所以文档还是很重要，我们经历了没有文档记录的痛楚，当你搞不清具体实现不得不仔细再看一遍痛苦的源码，这时候内心几乎是崩溃的...我在做什么？为什么我没有得到一个好的指引？

但是我们也经历过需求变更文档和代码不断变更最终维护困难难以一致的痛苦。像我们传统的开发流程，需求整理需求的文档，开发整理开发的文档，测试整理测试的文档，分支多版本多，很难搞清楚`需求-代码-测试案例`符合的版本。后面又面临分布式应用，跨部门合作，接口的联调开发测试都哭了...坦然，我自己也经历过两边的接口各按自己的想法设计，最后发现在一起的时候不是想象的样子...

所以有了很多文档的产生，包括像javadoc，java最古老的手段之一吧？利用注释生成api文档，当然开发写的注释谁写谁知道...而且老实讲除了jdk和比较优秀的开源项目，提供代码的同时也提供了javadoc供参考。（这里讲一下阿里爸爸开发规范是有要求要写注释的噢，字段&方法）。但是javadoc还是比较适合开发阅读吧，引用第三方的东西时总要了解下是否合适。

...这里省略一堆字吧，本意不是想那么多说为什么要用这个，而是我们怎么用好它。现在前后端分离的大环境下，自身应用的前后端接口一致，api文档可以给我们提供很大的帮助。（所以以前都是前后自己写...也没怎么太想过，现在是接触到了）

其他类似的api工具也很多啊，这里也不做对比了。主要一部分原因也是工作中涉及到了，自身想更了解一点。

> 其实就目前来看，个人觉得swagger侵入代码也还算比较严重，正常情况我们用api文档还是在开发测试阶段，生产时应该关闭掉，这部分引用的jar包是否能去掉呢？因为比较可读的文档还是会在注解里说明细一点。
>
> 或者代码见名知意？O(∩_∩)O哈哈~不要只知道复制粘贴啊....

# 1. swagger使用

现在基本都是spring boot，spring cloud应用了，就算不是也已经在往这方面做了，基本就结合spring boot注解使用了

## 1.1 maven引入

```
        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>knife4j-spring-boot-starter</artifactId>
            <version>2.0.3</version>
        </dependency>
```

这里因为工作的原因，了解到knife4j，开始是做swagger的ui。这个starter集成了相关的springfox-swagger ， knife4j-ui等等

```
# 太痛苦了，一直网页访问不了，后来了解到是和enablewebmvc注解冲突了，springboot用了autoconfiguration会自动加载资源，enablewebmvc需要手动加载，禁用掉
```

## 1.2 配置类

```java
package examples.config.api;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author wenxy
 * @create 2020-05-27 027
 */

@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfig {
    
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2);
    }
}

```

## 1.3 测试controller

```java
package examples.example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wenxy
 * @create 2020-05-28 028
 */
@RestController
@Api(tags = {"示例集合","示例"}, value = "示例控制器")
@RequestMapping("/example")
public class ExampleController {
    
    @ApiOperation(value = "示例方法",  notes = "this is a note for example method")
    @GetMapping("")
    public ResponseEntity<String> example(@ApiParam("描述信息") @RequestParam(value = "desc",defaultValue = "这是一个示例") String desc) {
        return ResponseEntity.ok(desc);
    }
    
}
```

## 1.4 启动应用

```java
package examples;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wenxy
 * @create 2020-05-24 024
 */
@SpringBootApplication
public class ExamplesApplication {
    
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ExamplesApplication.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }
}
```

