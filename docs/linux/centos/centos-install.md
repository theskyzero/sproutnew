[toc]



> centos安装配置

本来不是很想写这个的，百度一堆的教程吧，我觉得我也不会写得更好。但是有些小伙伴依赖性太强了，索性写一写吧，刚好最近瞎折腾，想装一个centos用来跑docker，记录一下安装过程

这里是用虚拟机安装的方式，硬盘安装是类似的了，区别是需要自己做启动盘就好了。~~不过我用的是`virtualbox`,vbox的话可能会简化一些安装的步骤，不像vmware,和硬盘装机基本一样（vmware更像一台虚拟的真实的机器，注意ubuntu不要使用简化模式）。总归都差不多，区别可能在于最开始的引导启动上，后面的安装是一致的。~~

终究还是让我放弃了vbox，想起一起走过的风风雨雨，免费小巧易用一直喜爱，但是弄个docker真的把我气得不行。

我用docker的想法是，虚拟机只是纯粹的虚拟机，尽量用主机保存大量的数据文件，比如docker下载的镜像，docker镜像涉及数据的文件比如数据库日志等等。当然也许全都放在虚拟机里也挺好，但是一不方便迁移扩展，二太占用虚拟机硬盘空间。反正我就是想这么做吧...之前在vmware上装ubuntu做个人开发环境，同时安装docker已经做到了，但是后面确实又被占用资源打败了...所以还是单纯的装个虚拟机跑docker，会更合适我这个人的小电脑。对于vbox我只能说一阵花里胡哨的操作下来，我成功的让vbox下的docker使用了之前vmware上docker下载的镜像，当时内心是惊喜的，然而...我tm一run centos就宕机了，一run就宕机，所以最终崩溃的放弃了

vmware毕竟还是更加稳定些，就是只能通过命令来无界面启动了...

# 1. 虚拟机安装centos7

```shell
安装程序： 
	virtualbox	6.1.8
	centos7		2003
```

## 1.1 下载镜像

可以去centos官网下载，也可以在各大开源镜像资源站下载。像操作系统镜像这种，我一般也都是直接找镜像站了，尤其最近比较爱阿里，顺便收集下[阿里的开源镜像站服务](https://developer.aliyun.com/mirror/)。

其他的，像[清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)、[华中科技大学开源镜像站](http://mirrors.hust.edu.cn/)、[北京理工大学开源镜像服务](http://mirror.bit.edu.cn/web/)...等等都是个不错的选择，咦我怎么全收集学校的，大概网易，oschina这些也有吧，搜一搜总有收获。

顺便说下，windows镜像我一般到[MSDN我告诉你](https://msdn.itellyou.cn/)下载，这里也可以下载windows的其他应用软件，需要个破解版的迅雷噢。

话不多说，我们的centos已经下载好了。

```shell
CentOS-7-x86_64-Minimal-2003.iso

# 因为安装在虚拟机，同时只想装个docker做服务器用，所以先选了minimal版本，这个比较小嘛，能让我们远程连接就好了，假装真的操作服务器环境...
```

## 1.2 安装virtualbox

哈，我写这个做什么，windows上装软件总会吧~

## 1.3 安装centos

### 1.3.1 创建虚拟机

vbox:

```yaml
新建虚拟机:
	名称: centos7
	文件夹: 
    类型: Linux
    版本: Red Hat (64-bit)

内存大小: 2048 MB

现在创建虚拟硬盘:
	虚拟硬盘文件类型: VDI
	存储在物理硬盘上: 动态分配
	文件位置和大小: 200 GB
```

vmware:

```yaml
新建虚拟机: 自定义高级

选择虚拟机硬件兼容性:
	虚拟机硬件兼容性: Workstation 15.x

安装客户机操作系统:
	安装程序光盘映像文件: CentOS-7-x86_64-DVD-2003.iso

命名虚拟机: 
	虚拟机名称: centos7
	位置: F:\Resources\Vm\Vmware\centos7

处理器配置:
	处理器数量: 2
	每个处理器内核数量: 2
	
此虚拟机内存: 2048 MB

网络连接: 使用网络地址转换nat

选择I/O控制器模型: LSI Logic # 推荐
	
选择磁盘类型: SCSI	# 推荐

选择磁盘: 
	创建新虚拟机磁盘: 
		最大磁盘大小: 200 GB 
		将虚拟机拆成多个文件: true
		磁盘文件: centos7.vmdk

完成	# 这里先不添加第二网卡
```



### 1.3.2 安装centos

```yaml
启动: 
	选择启动盘:
		注册: CentOS-7-x86_64-Minimal-2003.iso

# 进入安装界面
Install centos7:	# 这里不小心按了个enter，没注意默认选了哪个，问题不大

# 进入安装初始欢迎界面，这里是图形化的
语言:
	中文	简体中文（中国）	# 右上角键盘布局us改不了，选中文继续就好了，下面看到布局自动换成了cn
本地化:	# 默认即可
软件:		# 默认即可
系统:
	安装位置: # 因为是虚拟机，也没有手动升级的打算，默认自动分配
	Kdump: 关闭	# 系统崩溃处理，占用一部分内存，不需要，关闭了
	网络和主机名: 开启
	Security Policy: 关闭	# 虽然不知道拦截了什么，但是自己的虚拟机关闭安全策略就对了
用户设置:
	Root密码: root/root	# 好像可以不用设置，所以我还是设置了
	创建用户: wenxy/wenxy # 管理员账户
	
# 装好了呦，还真是激动人心呢
```

## 1.4 启动centos

根据提示重启，然后就进入了激动人心的，等等，命令行界面？

是不是傻？最小化安装本来就没有桌面...

```shell
# 是的，没看错
# centos7切换命令行和图形界面

# 图形 
systemctl set-default graphical.target

# 终端
systemctl set-default multi-user.target

# 查看当前方式
systemctl get-default 

# 没错，我现在看到的就是multi-user.target...
```

不过，这不刚好符合我的本意嘛，O(∩_∩)O哈哈~

## 1.5 配置远程连接

这里我用的是`SecureCRSecureFXPortable`二合一的破解版工具。

配置远程连接也就是主机要能连接虚拟机了，有兴趣的小伙伴可以多多了解下网络的几种模式，host only, net, bridge...

我就不多说了，个人用的都是双网卡，给虚拟机分配2个网卡就好了，一个`nat`供虚拟机上网，一个`host-only`供主机连接虚拟机（别人能不能通过主机连接我的虚拟机我才不管那么多呢）

```
1. 关机
2. 设置，网卡增加一个仅主机模式网卡
3. 重启
4. 登录
5. 使用 ip addr 查看ip
6. 使用securefx连接
7. 远程登录成功

# 网络这些东西默认都有还是比较方便的
```

OK，到此虚拟机安装centos完成。和ubuntu有点区别的是过程中没有换源，希望我们后面弯路也不要走的太多...

> vmware的nat就直接可以连ssh

# 2. 安装vmtools

虚拟机还得安装vmtools才行...后面可以挂载共享文件夹...

## 2.1 挂载vmtools

我们现在只有命令行，比较痛苦一点...

vbox:

```shell
# 挂载cdrom
设备->安装增强功能

# 查看cdrom
ls /dev

# 挂载出来cdrom
mkdir /mnt/cdrom
mount -t iso9660 /dev/cdrom /mnt/cdrom

# 查看，我这里跟网上不一样，直接看到安装脚本VBoxLinuxAdditions.run,直接运行会少东西
    ls /mnt/cdrom/

# 安装bzip2
yum install bzip2

# 升级内核，安装编译环境
yum update kernel -y
yum install kernel-headers kernel-devel gcc make -y

# 重启
reboot

# 切换root，继续执行
 /sbin/rcvboxadd setup
 
 # 看提示信息，貌似vmtools是要编译到内核里去的，不像windows直接安装就可以了，所以得要合适的内核版本和gcc编译工具
```

这时候，挂载共享文件夹到`/mnt/hgfs/F`就可以看到了



vmware:

```shell
虚拟机:
	重新安装vmware tools

# 以下使用root用户登录操作

# 挂载cdrom
mkdir /mnt/cdrom
mount /dev/cdrom /mnt/cdrom

# 解压
mkdir /tmp/vmtools
cd /tmp/vmtools
tar zxpf /mnt/cdrom/VMwareTools-10.3.21-14772444.tar.gz

# 安装
 ./vmware-tools-distrib/vmware-install.pl 
 
 # 这个安装好简单，一路点下去就行了，怎么感觉已经装过了...试了下共享文件夹，正常
```

ok，就这样了