[toc]

# Manjaro设置

----

主要含以下：

1. 换源
2. 常用软件安装（java）
3. 系统优化



作为一个develop，有些要求并不高，主要安装了java开发相关的软件，作为日常使用。

安装了manjaro的xfce版本，使用的是xfc4桌面。对桌面要求还是没那么高（想要个360桌面助手也没有吖）

# 1. 换源

开源的镜像&应用安装包，大多默认的是国外的源，国内不管是更新系统还是安装软件可能都会比较慢。

不过这里其实主要还是说软件的源，系统都已经装好了，系统附带的一些系统软件、应用软件需要更新和安装时，我们调用命令更新、安装时就会从软件源获取。

```shell
# 新增manjaro源， $arch=x86_64，$repo大概是pacman.conf定义的源title
# 搜索china镜像源排序
sudo pacman-mirrors -i -c China -m rank

# 列出China的源，可以选一个/多个,阿里源不太稳定
  0.745 China          : https://mirrors.aliyun.com/manjaro/
  0.376 China          : https://mirrors.ustc.edu.cn/manjaro/
  0.384 China          : https://mirrors.tuna.tsinghua.edu.cn/manjaro/
  0.298 China          : https://mirrors.sjtug.sjtu.edu.cn/manjaro/

# 保存选择，可以看到选择的源保存到了以下2个文件
# /var/lib/pacman-mirrors/custom-mirrors.json
# /etc/pacman.d/mirrorlist
::INFO Custom mirror file saved: /var/lib/pacman-mirrors/custom-mirrors.json
::INFO Writing mirror list
::China           : https://mirrors.ustc.edu.cn/manjaro/stable/$repo/$arch
::China           : https://mirrors.tuna.tsinghua.edu.cn/manjaro/stable/$repo/$arch
::China           : https://mirrors.sjtug.sjtu.edu.cn/manjaro/stable/$repo/$arch
::INFO Mirror list generated and saved to: /etc/pacman.d/mirrorlist
```

```shell
# 新增arch源
sudo mousepad /etc/pacman.conf
# xfce用的mousepad编辑器，也可以用终端编辑器还有vi,nano，vi不太好用

# 增加以下arch源
[archlinuxcn]
SigLevel = Optional TrustedOnly
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch

[antergos]
SigLevel = TrustAll
Server = https://mirrors.tuna.tsinghua.edu.cn/antergos/$repo/$arch

[arch4edu]
SigLevel = TrustAll
Server = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch
```

# 2. 软件安装

Arch默认的包管理器是pacman，不过pacman只支持搜索官方的应用。Arch社区仓库Arch User Repository（常被称作 AUR）用来文用户提供发布和使用其他的软件。

可以用来搜索安装aur软件的程序，一般叫aur助手。有很多，曾经最流行的，包括网上基本可见的就是yaourt，yaourt提供了对pacman的封装，简单来说，支持搜索官方应用和aur应用。

不过yaourt[有篇文章](https://linux.cn/article-9925-1.html?pr)说开发进度缓慢，推荐使用其他的如aurman、yay等aur助手。

我们也用yay好了，后面也直接用yay来安装软件。当然使用yay的换成其他aur助手讲道理也是可以的，本质都是用来搜索应用仓库的。

这里后面注意到，在1换源中配源的时候配置了应用仓库，在manjaro的软件仓库即可搜索在这些仓库内的软件。不过像搜狗拼音在软件仓库搜不到使用aur可以搜到，可能是还有其他的搜索位置

## 2.1 安装aur助手-yay

```shell
# 安装yay
sudo pacman -S yay

# 更新下
yay -Syy && yay -S archlinuxcn-keyring
```

## 2.2 安装软件

安装了办公用的的搜狗输入法、chroium浏览器、wps、Typora。

开发用的idea、datagrip、docker

一般用的微信、Tim

有了aur助手其实基本上安装软件只要输一条安装命令就可以了，相关的依赖也会被检测安装。当然使用manjaro自带的软件仓库也能搜到上面配置的各种软件仓库源中的软件。

### 2.2.1 搜狗输入法

网上都是pacman，但是fcitx-sogoupinyin搜不到，换成aur助手就可以了，显示有问题，补上最后的fcitx-qt4就可以了

```shell
# 安装
yay -S fcitx-im 
yay -S fcitx-configtool
yay -S fcitx-sogoupinyin
yay -S fcitx-qt4
```

```shell
# 配置
sudo mousepad ~/.profile

export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

注销重新登录，配置【fcitx配置】

```yaml
输入法: 
	键盘-汉语
	搜狗拼音
# 第一个输入法为非激活状态，通常将键盘放在第一位（按shift可以切换）
```

### 2.2.2 chromium浏览器

使用manjaro自带的软件管理或命令安装。

```shell
# 安装
yay -S chromium
```

```yaml
# 配置

# 安装Ghelper http://googlehelper.net/
# 登录账号即可使用google搜索，同步账号

#  登录账号，同步书签
```

### 2.2.3 wps

使用manjaro自带的软件管理或命令安装。

```shell
# 安装
yay -S wps-office-cn
yay -S ttf-wps-fonts
```

### 2.2.4 typora

很好用的markdwn编辑器。

使用manjaro自带的软件管理或命令安装。

```shell
# 安装
yay -S typora
```

### 2.2.5 idae

软件仓库里也可以搜得到。不过装了破解版就额外下了Linux低版本安装包+agent.jar破解，当然还是用ultimate版本好了。不太多说，文件版解压运行idea.sh即可。

下载地址：www.ypojie.com

### 2.2.6datagrip

同idea，额外下载压缩包安装

### 2.2.7 docker

使用manjaro自带的软件管理或命令安装。配置部分也可以详见docker-setting.md

```shell
# 安装
yay -S docker
```

docker本身镜像，运行等可能占用比较大的空间，另外也是直接和windows下虚拟机共用一个本地镜像仓库，节省占用系统盘空间。

```shell
# 设置路径
sudo mousepad /usr/lib/systemd/system/docker.service 

ExecStart=/usr/bin/dockerd --data-root=/run/media/theskyzero/File/Resources/Tools/docker/ --storage-driver=devicemapper -H fd:// --containerd=/run/containerd/containerd.sock
```

```shell
# 设置普通用户执行
sudo groupadd docker
sudo gpasswd -a ${USER} docker
newgrp docker
```

docker换源,使用了阿里[容器镜像服务]

```shell
# 换源
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://r2vk8r09.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 2.2.8 微信/Tim/钉钉

微信/Tim，腾讯是没有Linux版本的，需要基于wine使用，安装deepin提供的wine版本。

dingtalk既没有Linux版，deepin也没提供wine版，看到是有的用户基于网页版和electron制作的版本？传在了aur上，感觉不是太好...放弃了

```shell
# wechat
yay -S deepin-wine-wechat

# tim
yay -S deepin-wine-tim
```



> 放弃了的钉钉
>
> ```shell
> # 钉钉
> yay -S electron
> yay-S dingtalk#钉钉
> yay -S electron
> yay-S dingtalk
> ```

# 3. 系统优化

使用xfce也还没什么太好设置的，kde可能可玩性更高点吧，它提供了个下载主题的地方，xfce只能自己下载丢到某些目录下，然后在设置里选择就好了。

窗口管理器，外观啥的随便选了个暗色主题。换个漂亮的桌面，没了。

## 3.1 终端

不能忍受的就是终端超丑的字体间隔，其实确实是字体的问题。

[编辑]-[首选项]-[外观]-[字体]，使用系统字体。

```shell
# 百度贴吧找的下面这2个
sudo yay -S wqy-bitmapfont
sudo yay -S wqy-zenhei
```

## 3.2 双系统引导启动项

kde的版本装完，win10还能找到引导，xfce不行了，开机时直接进入了

```shell
# 开机选择
sudo sed -i -e "s/^GRUB_TIMEOUT_STYLE=hidden/#GRUB_TIMEOUT_STYLE=hidden/" /etc/default/grub
```

```shell
# 开机菜单
sudo mousepad /boot/grub/grub.cfg 

menuentry 'Manjaro Linux' --class manjaro --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-9bc4ba81-be93-4eaf-9d5e-83d06f4917d0' {
	savedefault
	load_video
	set gfxpayload=keep
	insmod gzio
	insmod part_gpt
	insmod ext2
	set root='hd1,gpt7'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root --hint-ieee1275='ieee1275//disk@0,gpt7' --hint-bios=hd1,gpt7 --hint-efi=hd1,gpt7 --hint-baremetal=ahci1,gpt7  9bc4ba81-be93-4eaf-9d5e-83d06f4917d0
	else
	  search --no-floppy --fs-uuid --set=root 9bc4ba81-be93-4eaf-9d5e-83d06f4917d0
	fi
	linux	/boot/vmlinuz-5.7-x86_64 root=UUID=9bc4ba81-be93-4eaf-9d5e-83d06f4917d0 rw  quiet apparmor=1 security=apparmor udev.log_priority=3
	initrd	/boot/intel-ucode.img /boot/initramfs-5.7-x86_64.img
}
```

## 3.3 开机挂载ntfs硬盘

```shell
# 查看硬盘信息
sudo fdisk -l

# 查看硬盘uuid
ls -l /dev/disk/by-uuid/

# 增加挂载
sudo mousepad /etc/fstab

# 最后的0 0 表示开机不检查硬盘
```

