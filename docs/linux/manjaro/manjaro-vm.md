[toc]

# Manjaro安装虚拟机

----

摆脱不了windows呀，还是以来一些gui工具，所以装个虚拟机



不管是vitualbox还是vmware，在linux上都要安装对应内核的版本才行，需要编译到内核里去的。所以安装之前，需要更新好系统，下载安装linux-headers

```shell
yay -Syyu

yay -S linux-headers
```



# 1. virtualbox



```shell
# virtualbox
yay -S virtualbox
# 选择对应内核的virtualbox-host-modules
yay -S linux57-virtualbox-host-modules
# virtualbox-ext-oracle
yay -S virtualbox-ext-oracle

# 
sudo modprobe vboxdrv
```

# 2. vmware-sorkstation

```shell
# virtualbox
yay -S virtualbox
# 选择对应内核的virtualbox-host-modules
yay -S linux57-virtualbox-host-modules
# virtualbox-ext-oracle
yay -S virtualbox-ext-oracle

# 
sudo modprobe vboxdrv
```

