package examples;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wenxy
 * @create 2020-05-24 024
 */
@SpringBootApplication
public class ExamplesApplication {
    
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ExamplesApplication.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }
}
