package examples.config.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * @author wenxy
 * @create 2020-05-24 024
 */
@Configuration
@Slf4j
public class RedisMessageConfig {
    
/*    @Bean
    public MessageListenerAdapter messageListenerAdapter() {
        return new MessageListenerAdapter(new MessageListener() {
            @Override
            public void onMessage(Message message, byte[] bytes) {
                Map<String, String> msg = new HashMap<String, String>(3);
                msg.put("topic", new String(bytes));
                msg.put("message.body", new String(message.getBody()));
                msg.put("message.channel", new String(message.getChannel()));
                log.info("receive message: {1}", JSON.toJSONString(msg));
            }
        });
    }
    
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter, Collection<Topic> topics) {
        log.info("register topics: {1}", topics);
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, topics);
        return container;
    }
    
    public static final Topic TEST_PATTEN_TOPIC = PatternTopic.of("test");*/
}
