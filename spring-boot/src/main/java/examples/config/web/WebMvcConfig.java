package examples.config.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author wenxy
 * @create 2020-05-24 024
 * // @EnableWebMvc 这个不能用会和springboot自动配置冲突，无法自动寻找web资源
 */
@Configuration

public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

    }
}
