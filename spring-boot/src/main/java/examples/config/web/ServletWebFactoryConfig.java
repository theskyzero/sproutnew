package examples.config.web;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wenxy
 * @create 2020-05-24 024
 */
@Configuration
public class ServletWebFactoryConfig {

    @Bean
    public ConfigurableServletWebServerFactory servletWebServerFactory() {
        return new TomcatServletWebServerFactory();
    }
}
