package examples.config.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

/**
 * @author wenxy
 * @create 2020-06-29 029
 */
@EnableAdminServer
@Configuration
@ConditionalOnProperty(prefix = "actuator.view",name = "enable",havingValue = "true",matchIfMissing = true)
public class SpringBootAdminConfig {}
