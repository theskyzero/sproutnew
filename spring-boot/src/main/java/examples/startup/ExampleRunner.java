package examples.startup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author wenxy
 * @create 2020-05-24 024
 */
@Configuration
@Slf4j
public class ExampleRunner {

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> log.info("command line runner run with args: {}", args);
    }

    @Bean
    public ApplicationRunner applicationRunner() {
        return args -> log.info("application runner run with args: {}", args);
    }


    public static void main(String[] args) throws NoSuchMethodException, ClassNotFoundException {

        Method method = ExampleRunner.class.getMethod("test", null);
        Class c = Class.forName(method.getGenericReturnType().toString());

        System.err.println(c.getSigners());
    }

    public List<ExampleRunner> test() {
        return null;
    }
}
