package examples.startup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.AvailabilityState;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

/**
 * @author wenxy
 * @date 2020-05-24 024
 */
@Slf4j
@Configuration
public class ExampleAvailability {

    @EventListener
    public void onStateChange(AvailabilityChangeEvent<AvailabilityState> event) {
        log.info("state changed to : timestamp={},state={},source={}", event.getTimestamp(), event.getState(), event.getSource());
    }
}
