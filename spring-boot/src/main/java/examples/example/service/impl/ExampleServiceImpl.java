package examples.example.service.impl;

import examples.example.service.ExampleService;
import org.springframework.context.ApplicationContext;

/**
 * @author wenxy
 * @create 2020-06-28 028
 */

public class ExampleServiceImpl implements ExampleService {
    
    ApplicationContext context;
    
    
    @Override
    public void trans() {
    
    }
    
    public void setContext(ApplicationContext context) {
        this.context = context;
    }
}
