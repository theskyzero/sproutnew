package examples.example.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wenxy
 * @create 2020-05-28 028
 */
@RestController
@Api(tags = {"示例集合","示例"}, value = "示例控制器")
@RequestMapping("/example")
public class ExampleController {
    
    @ApiOperation(value = "示例方法",  notes = "this is a note for example method")
    @GetMapping("")
    public ResponseEntity<String> example(@ApiParam("描述信息") @RequestParam(value = "desc",defaultValue = "这是一个示例") String desc) {
        return ResponseEntity.ok(desc);
    }
    
    public ResponseEntity<String> trans(@ApiParam("描述信息") @RequestParam(value = "desc",defaultValue = "这是一个示例") String desc){
        return ResponseEntity.ok(desc);
    }
    
}
