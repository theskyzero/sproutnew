[TOC]

# 小白养成记

#### 介绍
萌新上路

记录日常的点点滴滴

#### 使用说明



# 1.

# 2. 



# 3. 工具

## 3.1 版本控制

### 3.1.1 Git

- [仓库SSH公钥配置](docs/vcs/git/git-ssh.md)

## 3.2 构建工具

### 3.2.1 Maven



## 3.3 执行器

## 3.4 部署工具

### 3.4.1 docker

- [安装](docs/docker/docker-install.md)
- [配置](docs/docker/docker-settings.md)
- 软件安装
  - [mysql](docs/docker/docker-mysql.md)
  - [redis](docs/docker/docker-redis.md)
- 



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
