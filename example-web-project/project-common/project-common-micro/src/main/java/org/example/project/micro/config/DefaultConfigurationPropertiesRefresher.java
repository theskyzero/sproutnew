package org.example.project.micro.config;

import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import org.example.project.micro.util.ConfigurationRefreshUtils;

/**
 * @author wenxy
 * @date 2020/11/9
 */
public class DefaultConfigurationPropertiesRefresher {

    public void refreshConfigurationProperties(ConfigChangeEvent configChangeEvent) {
        ConfigurationRefreshUtils.refreshCustom(configChangeEvent);
    }
}
