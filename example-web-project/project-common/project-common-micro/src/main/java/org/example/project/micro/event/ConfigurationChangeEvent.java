package org.example.project.micro.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.core.env.Environment;

import java.util.Set;

/**
 * Event published to signal a change in the {@link Environment}.
 *
 * @author Dave Syer
 */
@SuppressWarnings("serial")
public class ConfigurationChangeEvent extends ApplicationEvent {

    private Set<String> keys;

    public ConfigurationChangeEvent(Set<String> keys) {
        this(keys, keys);
    }

    public ConfigurationChangeEvent(Object context, Set<String> keys) {
        super(context);
        this.keys = keys;
    }

    /**
     * @return The keys.
     */
    public Set<String> getKeys() {
        return this.keys;
    }

}
