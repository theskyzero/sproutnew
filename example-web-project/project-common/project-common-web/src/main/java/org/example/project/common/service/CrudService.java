package org.example.project.common.service;

/**
 * crud service for single table
 *
 * @author theskyzero
 * @date 2020/9/25
 */
public interface CrudService<DTO, ID> {

    /**
     * save dto
     *
     * @param dto data transfer object
     * @return dto
     */
    DTO save(DTO dto);

    /**
     * save on duplicate key update
     *
     * @param dto
     * @return
     */
    default DTO saveUpdate(DTO dto) {
        throw new UnsupportedOperationException();
    }

    /**
     * findDtoById
     *
     * @param id identifier
     * @return dto
     */
    DTO getById(ID id);

    /**
     * updateDtoById
     *
     * @param dto data transfer object
     * @param id  identifier
     * @return updateById count
     */
    default int updateById(DTO dto, ID id) {
        throw new UnsupportedOperationException();
    }

    /**
     * removeDtoById
     *
     * @param id identifier
     * @return removeById count
     */
    default int removeById(ID id) {
        throw new UnsupportedOperationException();
    }
}
