package org.example.project.common.error.exception;

import lombok.Data;
import org.example.project.common.error.MessageEnum;

/**
 * 业务自定义异常
 * <p>
 * 兼容旧异常 UcException/OcException 增加，
 * 后续改造完可考虑去除，使用通用的ServiceException做service层日志，DaoException做第三方日志
 *
 * @author wenxy
 * @date 2020/11/6
 */
@Data
public class BizCustomException extends RuntimeException {

    MessageEnum messageEnum;

    public BizCustomException(MessageEnum messageEnum) {
        this.messageEnum = messageEnum;
    }

    public BizCustomException(String s, MessageEnum messageEnum) {
        super(s);
        this.messageEnum = messageEnum;
    }

    public BizCustomException(String s, Throwable throwable, MessageEnum messageEnum) {
        super(s, throwable);
        this.messageEnum = messageEnum;
    }

    public BizCustomException(Throwable throwable, MessageEnum messageEnum) {
        super(throwable);
        this.messageEnum = messageEnum;
    }

    public Boolean isClientError() {
        return true;
    }
}
