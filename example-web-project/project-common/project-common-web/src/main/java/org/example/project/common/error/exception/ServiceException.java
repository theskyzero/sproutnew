package org.example.project.common.error.exception;


import org.example.project.common.error.MessageEnum;

/**
 * ServiceException
 *
 * @author wenxy
 * @date 2020/9/28
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -2351852312234556372L;

    MessageEnum messageEnum;

    public ServiceException(MessageEnum messageEnum, String s) {
        super(s);
        this.messageEnum = messageEnum;
    }

    public ServiceException(MessageEnum messageEnum, String s, Throwable throwable) {
        super(s, throwable);
        this.messageEnum = messageEnum;
    }

    public ServiceException(MessageEnum messageEnum, Throwable throwable) {
        super(throwable);
        this.messageEnum = messageEnum;
    }

    public MessageEnum getMessageEnum() {
        return messageEnum;
    }

    public void setMessageEnum(MessageEnum messageEnum) {
        this.messageEnum = messageEnum;
    }
}
