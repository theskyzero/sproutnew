package org.example.project.common.util;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author 13360
 * @see Jdk8DateUtils 建议使用LocalDateTime
 */
@Deprecated
public class DateUtils {

    /**
     * 获取当前 时:分:秒   HH:mm:ss 24制
     */
    public final static String DATE_FORMAT_TIME24 = "HH:mm:ss";

    /**
     * 获取当前 时:分:秒   HH:mm:ss 24制
     */
    public final static String DATE_FORMAT_TIME12 = "hh:mm:ss";

    /**
     * 获取当前 年-月-日 时:分:秒   yyyy-MM-dd HH:mm:ss 24制
     */
    public final static String DATE_FORMAT_DATE_TIME24 = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获取当前 年-月-日 时:分:秒   yyyy-MM-dd HH:mm:ss 12制
     */
    public final static String DATE_FORMAT_DATE_TIME12 = "yyyy-MM-dd hh:mm:ss";

    /**
     * 秒
     */
    public final static long TIMES_SECOND = 1000L;

    /**
     * 分
     */
    public final static long TIMES_MINUTE = TIMES_SECOND * 60L;

    /**
     * 时
     */
    public final static long TIMES_HOUR = TIMES_MINUTE * 60L;

    /**
     * 天
     */
    public final static long TIMES_DAY = TIMES_HOUR * 24L;

    /**
     * 周
     */
    public final static long TIMES_WEEK = TIMES_DAY * 7L;

    /**
     * 获取当日信息,作为redis key的一部分,统计每日命中率
     *
     * @return
     */
    public static String getTodayYYYYMMDD() {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        //当日的年月日
        String todayYYYYMMDD = sdf.format(calendar.getTime());
        return todayYYYYMMDD;
    }

    /**
     * 获取指定日期之前、之后的日期，日期格式yyyyMMdd
     *
     * @param date 当前日期
     * @param n    天
     * @return
     */
    public static String getSpecialDate(String date, int n) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(date));
        } catch (ParseException e) {
            return null;
        }
        //设置之前之后的天数
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + n);
        //获取新的时间
        String todayYYYYMMDD = sdf.format(calendar.getTime());
        return todayYYYYMMDD;
    }

    /**
     * 获取指定日期之前、之后的日期，日期格式yyyyMMdd
     *
     * @param date 当前日期
     * @param n    天
     * @return
     */
    public static String getSpecialDate(String date, int n, String pattern) {
        if (StringUtils.isBlank(pattern)) {
            pattern = "yyyyMMdd";
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(date));
        } catch (ParseException e) {
            return null;
        }
        //设置之前之后的天数
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + n);
        //获取新的时间
        String today = sdf.format(calendar.getTime());
        return today;
    }

    /**
     * 以指定格式获取当前时间
     *
     * @param pattern
     * @return
     */
    public static String getCurrentTimeByPattern(String pattern) {

        //时间格式为空返回yyyyMMdd
        if (StringUtils.isEmpty(pattern)) {
            return getTodayYYYYMMDD();
        }

        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Calendar calendar = Calendar.getInstance();

        //当前时间
        String currTime = sdf.format(calendar.getTime());
        return currTime;
    }

    /**
     * 获取两个时间相隔时间 毫秒
     *
     * @param src
     * @param dis
     * @param pattern
     * @return
     */
    public static long getRangeValues(String src, String dis, String pattern) throws ParseException {

        //获取Date对象
        Date srcDate = getDate(src, pattern);
        Date disDate = getDate(dis, pattern);

        //获取Calendar对象
        Calendar srcC = Calendar.getInstance();
        srcC.setTime(srcDate);

        Calendar disC = Calendar.getInstance();
        disC.setTime(disDate);

        //获取时间戳
        Long srcL = srcC.getTimeInMillis();
        Long disL = disC.getTimeInMillis();

        return srcL - disL;

    }

    /**
     * 以特定的格式解析时间获取date对象
     *
     * @param date
     * @param pattern
     * @return
     * @throws ParseException
     */
    public static Date getDate(String date, String pattern) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.parse(date);
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(Calendar.getInstance().getTime());
        return currentTime;
    }
}
