package org.example.project.common.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * domain object
 * <p>
 * usually as persistent object
 *
 * @author theskyzero
 * @date 2020/9/23
 */
@Data
public class BaseDO implements Serializable {
    private static final long serialVersionUID = -8995370299071854890L;

    Long id;
    LocalDateTime createTime;
    LocalDateTime updateTime;
}
