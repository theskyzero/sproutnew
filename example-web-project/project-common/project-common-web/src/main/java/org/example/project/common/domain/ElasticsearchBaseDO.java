package org.example.project.common.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * Elasticsearch domain基类
 *
 * @author wenxy
 * @date 2020/10/23
 */
@Data
public class ElasticsearchBaseDO implements Serializable {

    String id;

    Long version;
}
