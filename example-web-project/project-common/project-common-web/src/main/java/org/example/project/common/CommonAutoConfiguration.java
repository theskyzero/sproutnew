package org.example.project.common;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author wenxy
 * @date 2020/11/12
 */
@ComponentScan
public class CommonAutoConfiguration {
}
