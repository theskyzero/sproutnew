package org.example.project.common.web.mapper;


import org.example.project.common.service.mapper.BaseDtoMapper;

import java.util.List;

/**
 * 数据传输对象和展示层对象映射器
 * <p>
 * mapper between view object and data transform object
 * <p>
 * 其实和BaseDtoMapper一样，方便理解分开命名
 *
 * @author wenxy
 * @date 2020/10/10
 * @see BaseDtoMapper
 */
public interface BaseVoMapper<VO, DTO> {

    /**
     * vo2Dto
     *
     * @param vo vo
     * @return dto
     */
    DTO toDTO(VO vo);

    /**
     * vo2Dto
     *
     * @param voList voList
     * @return dtoList
     */
    List<DTO> toDTO(List<VO> voList);

    /**
     * dto2Vo
     *
     * @param dto dto
     * @return vo
     */
    VO toVO(DTO dto);

    /**
     * dto2Vo
     *
     * @param dtoList dtoList
     * @return voList
     */
    List<VO> toVO(List<DTO> dtoList);
}
