package org.example.project.common.error;

/**
 * 错误码接口
 *
 * @author xiaogm
 * @version 1.0
 * @date 2019/8/21 14:23
 */
public interface MessageEnum {
    /**
     * 错误码
     *
     * @return
     */
    String code();

    /**
     * 错误信息
     *
     * @return
     */
    String msg();

    /**
     * 用户端错误
     *
     * @return
     */
    default Boolean isClientError() {
        return false;
    }
}
