package org.example.project.common.error.exception;


import org.example.project.common.error.MessageEnum;

/**
 * DaoException
 * <p>
 * 覆盖调用第三方的日志
 *
 * @author wenxy
 * @date 2020/9/28
 */
public class DaoException extends RuntimeException {

    private static final long serialVersionUID = -2104194689457789223L;

    MessageEnum messageEnum;

    public DaoException(MessageEnum messageEnum, String s) {
        super(s);
        this.messageEnum = messageEnum;
    }

    public DaoException(MessageEnum messageEnum, String s, Throwable throwable) {
        super(s, throwable);
        this.messageEnum = messageEnum;
    }

    public DaoException(MessageEnum messageEnum, Throwable throwable) {
        super(throwable);
        this.messageEnum = messageEnum;
    }

    public MessageEnum getMessageEnum() {
        return messageEnum;
    }

    public void setMessageEnum(MessageEnum messageEnum) {
        this.messageEnum = messageEnum;
    }
}
