package org.example.project.common.validator;

/**
 * crud 校验组
 *
 * @author wenxy
 * @date 2020/10/25
 */
public interface ValidateGroup {

    interface Create {
    }

    interface Retrieve {
    }

    interface Update {
    }

    interface Delete {
    }
}
