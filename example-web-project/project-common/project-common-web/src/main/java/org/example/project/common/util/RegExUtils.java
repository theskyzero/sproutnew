package org.example.project.common.util;

/**
 * 正则表达式工具类
 *
 * @author wenxy
 * @date 2020/10/12
 */
public class RegExUtils {

    /**
     * Email
     */
    public static final String EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

    /**
     * 手机号码
     */
    public static final String MOBILE = "^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$";

    /**
     * 电话号码("XXX-XXXXXXX"、"XXXX-XXXXXXXX"、"XXX-XXXXXXX"、"XXX-XXXXXXXX"、"XXXXXXX"和"XXXXXXXX)
     */
    public static final String TEL = "^(\\(\\d{3,4}-)|\\d{3.4}-)?\\d{7,8}$";

}
