package org.example.project.common.util;

import org.aspectj.lang.JoinPoint;
import org.example.project.common.contant.Log;

import java.util.Arrays;

/**
 * @author wenxy
 * @date 2020/10/26
 */
public class AspectLogUtils {

    public static Log buildLog(JoinPoint joinPoint) {
        return buildLog(joinPoint, null, null);
    }

    public static Log buildLog(JoinPoint joinPoint, Object result) {
        return buildLog(joinPoint, result, null);
    }

    public static Log buildLog(JoinPoint joinPoint, Throwable e) {
        return buildLog(joinPoint, null, e);
    }

    public static Log buildLog(JoinPoint joinPoint, Object result, Throwable e) {
        final Log.LogBuilder logBuilder = Log.simpleLogBuilder();
        return logBuilder
                .signature(joinPoint.toString())
                .param(Arrays.asList(joinPoint.getArgs()))
                .result(result)
                .exception(e)
                .build();
    }
}
