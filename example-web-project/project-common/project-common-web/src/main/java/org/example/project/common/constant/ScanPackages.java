package org.example.project.common.constant;

/**
 * @author wenxy
 * @date 2020/10/23
 */
public interface ScanPackages {
    String GLOBAL = "com.example";
    String PROJECT = "com.example.project";
}
