package org.example.project.common.service;

import java.util.List;

/**
 * crud service for sharded tables
 *
 * @author theskyzero
 * @date 2020/9/25
 */
public interface ShardCrudService<DTO, ID, SHARD> extends CrudService<DTO, ID> {

    /**
     * listByShardKey
     *
     * @param shard shard key
     * @return data transfer object
     */
    List<DTO> getByShardKey(SHARD shard);

    /**
     * removeByShardKey
     *
     * @param shard shard key
     * @return removeById count
     */
    default int removeByShardKey(SHARD shard) {
        throw new UnsupportedOperationException();
    }

    /**
     * updateByShardKey
     *
     * @param dto   data transfer object
     * @param shard shard key
     * @return updateById count
     */
    default int updateByShardKey(DTO dto, SHARD shard) {
        throw new UnsupportedOperationException();
    }

    /**
     * getById
     * <p>
     * unsupported for shard tables unless the shard key is identifier
     *
     * @param id identifier
     * @return data transfer object
     */
    @Override
    default DTO getById(ID id) {
        throw new UnsupportedOperationException();
    }

}
