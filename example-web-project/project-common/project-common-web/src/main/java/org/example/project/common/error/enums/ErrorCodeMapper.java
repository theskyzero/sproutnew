package org.example.project.common.error.enums;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author wenxy
 * @date 2020/10/26
 */
@Getter
public enum ErrorCodeMapper {

    /**
     * {@code 200 OK}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.3.1">HTTP/1.1: Semantics and Content, section 6.3.1</a>
     */
    OK(ErrorCode.SUCCESS, HttpStatus.OK),

    /**
     * {@code 400 Bad Request}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.5.1">HTTP/1.1: Semantics and Content, section 6.5.1</a>
     */
    BAD_REQUEST(ErrorCode.A0001, HttpStatus.BAD_REQUEST),
    /**
     * {@code 401 Unauthorized}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7235#section-3.1">HTTP/1.1: Authentication, section 3.1</a>
     */
    UNAUTHORIZED(ErrorCode.A0301, HttpStatus.UNAUTHORIZED),
    /**
     * {@code 403 Forbidden}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.5.3">HTTP/1.1: Semantics and Content, section 6.5.3</a>
     */
    FORBIDDEN(ErrorCode.A0312, HttpStatus.FORBIDDEN),
    /**
     * {@code 451 Unavailable For Legal Reasons}.
     *
     * @see <a href="https://tools.ietf.org/html/draft-ietf-httpbis-legally-restricted-status-04">
     * An HTTP Status Code to Report Legal Obstacles</a>
     * @since 4.3
     */
    UNAVAILABLE_FOR_LEGAL_REASONS(ErrorCode.A0430, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS),
    /**
     * {@code 405 Method Not Allowed}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.5.5">HTTP/1.1: Semantics and Content, section 6.5.5</a>
     */
    METHOD_NOT_ALLOWED(ErrorCode.A0440, HttpStatus.METHOD_NOT_ALLOWED),
    /**
     * {@code 406 Not Acceptable}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.5.6">HTTP/1.1: Semantics and Content, section 6.5.6</a>
     */
    /**
     * {@code 404 Not Found}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.5.4">HTTP/1.1: Semantics and Content, section 6.5.4</a>
     */
    NOT_FOUND(ErrorCode.A0500, HttpStatus.NOT_FOUND),
    /**
     * {@code 429 Too Many Requests}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc6585#section-4">Additional HTTP Status Codes</a>
     */
    TOO_MANY_REQUESTS(ErrorCode.A0501, HttpStatus.TOO_MANY_REQUESTS),
    /**
     * {@code 500 Internal Server Error}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.6.1">HTTP/1.1: Semantics and Content, section 6.6.1</a>
     */
    INTERNAL_SERVER_ERROR(ErrorCode.B0001, HttpStatus.INTERNAL_SERVER_ERROR),
    /**
     * {@code 503 Service Unavailable}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.6.4">HTTP/1.1: Semantics and Content, section 6.6.4</a>
     */
    SERVICE_UNAVAILABLE(ErrorCode.C0001, HttpStatus.SERVICE_UNAVAILABLE),
    /**
     * {@code 502 Bad Gateway}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-6.6.3">HTTP/1.1: Semantics and Content, section 6.6.3</a>
     */
    BAD_GATEWAY(ErrorCode.C0154, HttpStatus.BAD_GATEWAY),

    ;
    ErrorCode errorCode;
    HttpStatus httpStatus;

    ErrorCodeMapper(ErrorCode errorCode, HttpStatus httpStatus) {
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }


    public static HttpStatus toHttpStatus(ErrorCode errorCode) {
        for (ErrorCodeMapper mapper : ErrorCodeMapper.values()) {
            if (errorCode == mapper.getErrorCode()) {
                return mapper.getHttpStatus();
            }
        }

        if (errorCode.isClientError()) {
            return HttpStatus.BAD_REQUEST;
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
