package org.example.project.common.service.mapper;

import java.util.List;

/**
 * 领域对象和数据传输对象映射器
 * <p>
 * mapper between domain object and data transform object
 *
 * @author wenxy
 * @date 2020/10/10
 */
public interface BaseDtoMapper<DTO, DO> {

    /**
     * do2Dto
     *
     * @param domain domain
     * @return dto
     */
    DTO toDTO(DO domain);

    /**
     * do2Dto
     *
     * @param domainList domainList
     * @return dtoList
     */
    List<DTO> toDTO(List<DO> domainList);

    /**
     * dto2Do
     *
     * @param dto dto
     * @return do
     */
    DO toDO(DTO dto);

    /**
     * dto2Do
     *
     * @param dtoList dtoList
     * @return doList
     */
    List<DO> toDO(List<DTO> dtoList);
}
