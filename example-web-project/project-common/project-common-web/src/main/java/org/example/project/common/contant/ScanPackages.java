package org.example.project.common.contant;

/**
 * @author wenxy
 * @date 2020/10/23
 */
public interface ScanPackages {
    String GLOBAL = "com.yundasys";
    String PROJECT = "com.yundasys.vienna";
}
