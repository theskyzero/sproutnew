package org.example.project.common.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.example.project.common.domain.BaseDO;

/**
 * data transfer object
 * <p>
 * usually as domain object
 *
 * @author theskyzero
 * @date 2020/9/23
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BaseDTO extends BaseDO {
    private static final long serialVersionUID = -8733818337672821933L;
}
