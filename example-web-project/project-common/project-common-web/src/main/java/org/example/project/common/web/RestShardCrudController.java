package org.example.project.common.web;

import org.example.project.common.contant.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * rest crud controller for sharded tables
 *
 * @author theskyzero
 * @date 2020/9/28
 */
public interface RestShardCrudController<DTO, ID, SHARD> extends RestCrudController<DTO, ID> {

    /**
     * removeByShardKey
     *
     * @param shard shard key
     * @return Result<ResultCodeEnum.SUCCESS>
     * @see R
     */
    default R<String> removeByShardKey(@NotNull SHARD shard) {
        return R.success();
    }

    /**
     * updateByShardKey
     *
     * @param dto   dto
     * @param shard shard key
     * @return Result<ResultCodeEnum.SUCCESS>
     * @see R
     */
    default R<String> updateByShardKey(@Validated @RequestBody DTO dto, @NotNull SHARD shard) {
        return R.success();
    }

    /**
     * listDtoByShardKey
     *
     * @param shard shard key
     * @return Result<List < DTO>>
     * @see R
     */
    R<List<DTO>> getByShardKey(@NotNull SHARD shard);

    /**
     * 分表不支持根据非分表列查询
     *
     * @param id id
     * @return
     */
    @Override
    default R<DTO> getById(ID id) {
        throw new UnsupportedOperationException();
    }

}
