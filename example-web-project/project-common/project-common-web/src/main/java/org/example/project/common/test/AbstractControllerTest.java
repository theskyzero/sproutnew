package org.example.project.common.test;

import org.example.project.common.contant.R;
import org.example.project.common.error.enums.ErrorCode;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * Controller 测试基类
 * <p>
 * controller有2种测试方法，一种是通过spring容器bean调用，一种是发起http请求。
 * 后者测试会比较真实，此基类采用后者，通过restTemplate向web应用发起请求
 *
 * @author wenxy
 * @date 2020/11/12
 */
public abstract class AbstractControllerTest {

    protected RestTemplate restTemplate = new RestTemplate();
    protected UriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory();

    public AbstractControllerTest() {
        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

            }
        });
    }

    protected <T> void assertSuccess(ResponseEntity<R<T>> responseEntity) {
        Assert.assertTrue(
                responseEntity.getStatusCode().is2xxSuccessful()
                        && responseEntity.getBody().isSuccess()
                        && ErrorCode.isSuccess(responseEntity.getBody().getResCode())
        );
    }


    protected <T> void assertFail(ResponseEntity<R<T>> responseEntity) {
        Assert.assertTrue(
                responseEntity.getStatusCode().isError()
                        && responseEntity.getBody().notSuccess()
                        && !ErrorCode.isSuccess(responseEntity.getBody().getResCode())
        );
    }

    protected URI createURI(String url, Map<String, ?> params) {
        return uriBuilderFactory.expand(url, params);
    }

    protected URI createURI(String url, Object... uriVariables) {
        return uriBuilderFactory.expand(url, uriVariables);
    }

    protected <T> RequestEntity<T> createRequestEntity(HttpMethod method, String url, T body) {
        return RequestEntity.method(method, URI.create(url)).body(body);
    }

}