package org.example.project.common.openapi.constant;

/**
 * @author wenxy
 * @date 2020/10/21
 */
public interface SystemInfo {

    /**
     * 项目工程版本
     */
    String PROJECT_VERSION = "1.0.0";

    /**
     * 部门名称
     */
    String DEPARTMENT = "演示工程";

    String PROJECT_GIT = "";

    String EMAIL = "theskyzero@163.com";
}
