package org.example.project.common.openapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.swagger2.configuration.Swagger2DocumentationWebMvcConfiguration;

/**
 * @author wenxy
 * @date 2020/10/24
 */
@Configuration
@Import({
        Swagger2DocumentationWebMvcConfiguration.class,
        BeanValidatorPluginsConfiguration.class,
})
public class OpenApiAutoConfiguration {

}
