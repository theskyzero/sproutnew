package org.example.project.common.middleware.service;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchHitsIterator;
import org.springframework.data.elasticsearch.core.query.*;

import java.util.List;

/**
 * @author wenxy
 * @date 2020/10/21
 */
public interface ElasticsearchService {
    /**
     * 保存文档
     *
     * @param entity 文档
     * @param index  索引
     * @param <T>    文档类型
     * @return
     */
    <T> T save(T entity, String index);

    /**
     * 批量保存文档
     *
     * @param entities 文档
     * @param index    索引
     * @param <T>      文档类型
     * @return
     */
    <T> Iterable<T> save(Iterable<T> entities, String index);

    /**
     * 获取文档id
     *
     * @param query
     * @param index
     * @return
     */
    String index(IndexQuery query, String index);

    /**
     * 获取文档
     *
     * @param id
     * @param clazz
     * @param index
     * @param <T>
     * @return
     */
    <T> T get(String id, Class<T> clazz, String index);

    /**
     * 批量获取文档
     *
     * @param query 查询条件
     * @param clazz 文档类型
     * @param index 索引
     * @param <T>   文档类型
     * @return
     */
    <T> List<T> multiGet(Query query, Class<T> clazz, String index);

    /**
     * 文档是否存在
     *
     * @param id    文档id
     * @param index 索引
     * @return
     */
    boolean exists(String id, String index);

    /**
     * 批量获取文档id
     *
     * @param queries     查询条件
     * @param bulkOptions bulk操作
     * @param index       索引
     * @return
     */
    List<String> bulkIndex(List<IndexQuery> queries, BulkOptions bulkOptions, String index);

    /**
     * 批量更新
     *
     * @param queries     更新条件
     * @param bulkOptions bulk操作
     * @param index       索引
     */
    void bulkUpdate(List<UpdateQuery> queries, BulkOptions bulkOptions, String index);

    /**
     * 删除文档
     *
     * @param id    文档id
     * @param index 索引
     * @return
     */

    String delete(String id, String index);

    /**
     * 删除文档
     *
     * @param entity 文档
     * @param index  索引
     * @return
     */

    String delete(Object entity, String index);

    /**
     * 批量删除文档
     *
     * @param query 删除条件
     * @param clazz 文档类型
     * @param index 索引
     */

    void delete(Query query, Class<?> clazz, String index);

    /**
     * 统计文档数量
     *
     * @param query 查询条件
     * @param clazz 文档类型
     * @param index 索引
     * @return
     */

    long count(Query query, Class<?> clazz, String index);

    /**
     * suggestion
     *
     * @param suggestion suggestion
     * @param index      index
     * @return
     */

    SearchResponse suggest(SuggestBuilder suggestion, String index);

    /**
     * 多重搜索
     *
     * @param queries 查询条件
     * @param clazz   文档类型
     * @param index   索引
     * @param <T>     文档类型
     * @return 每个查询条件的查询结果
     */

    <T> List<SearchHits<T>> multiSearch(List<? extends Query> queries, Class<T> clazz, String index);

    /**
     * 多重搜索
     *
     * @param queries 查询条件
     * @param classes 文档类型
     * @param index   文档类型
     * @return
     */

    List<SearchHits<?>> multiSearch(List<? extends Query> queries, List<Class<?>> classes, String index);

    /**
     * 文档搜索
     *
     * @param query 搜索条件
     * @param clazz 文档类型
     * @param index 索引
     * @param <T>   文档类型
     * @return
     */

    <T> SearchHits<T> search(Query query, Class<T> clazz, String index);

    /**
     * 文档搜索
     *
     * @param query 搜索条件
     * @param clazz 文档类型
     * @param index 索引
     * @param <T>   文档类型
     * @return
     */

    <T> SearchHits<T> search(MoreLikeThisQuery query, Class<T> clazz, String index);

    /**
     * stream 搜索
     *
     * @param query 搜索条件
     * @param clazz 文档类型
     * @param index 索引
     * @param <T>   文档类型
     * @return
     */

    <T> SearchHitsIterator<T> searchForStream(Query query, Class<T> clazz, String index);


}
