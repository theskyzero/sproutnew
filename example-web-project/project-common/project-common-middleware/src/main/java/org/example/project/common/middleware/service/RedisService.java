package org.example.project.common.middleware.service;


import org.example.project.common.middleware.service.redis.*;

/**
 * 封装redis操作
 * <p>
 * value、list、set、hash、zSet、
 *
 * @author wenxy
 * @date 2020/9/29
 */
public interface RedisService<K, V> extends RedisClusterService<K, V>, RedisBaseService<K, V>, RedisValueService<K, V>, RedisListService<K, V>, RedisHashService<K, Object, V> {
}
