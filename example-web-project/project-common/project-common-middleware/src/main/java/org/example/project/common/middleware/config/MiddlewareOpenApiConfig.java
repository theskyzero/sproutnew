package org.example.project.common.middleware.config;

import org.example.project.common.middleware.constant.ApiRequestMapping;
import org.example.project.common.openapi.constant.SystemInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author wenxy
 * @date 2020/10/21
 */
@Configuration
public class MiddlewareOpenApiConfig {

    private static final String API_MIDDLEWARE_GROUP = "中间件API";

    @Value("${swagger.show:true}")
    boolean show = true;

    @Bean
    public Docket middlewareApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant(ApiRequestMapping.MIDDLEWARE_API + "/**"))
                .apis(RequestHandlerSelectors.any())
                .build()
                .apiInfo(middlewareApiInfo())
                .enable(show)
                .groupName(API_MIDDLEWARE_GROUP)
                ;
    }


    ApiInfo middlewareApiInfo() {
        return new ApiInfoBuilder()
                .title("维也纳通用组件-中间件模块API开放文档")
                .version(SystemInfo.PROJECT_VERSION)
                .description("中间件简单运维功能api服务，请谨慎使用!")
                .contact(new Contact(SystemInfo.DEPARTMENT, SystemInfo.PROJECT_GIT, SystemInfo.EMAIL))
                .build();
    }
}
