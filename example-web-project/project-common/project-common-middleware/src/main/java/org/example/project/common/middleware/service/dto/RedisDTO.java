package org.example.project.common.middleware.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author wenxy
 * @date 2020/10/9
 */
@Data
public class RedisDTO implements Serializable {

    private static final long serialVersionUID = 7413698670415561523L;
    @NotBlank
    String key;
    Object value;

    public RedisDTO() {
    }

    public RedisDTO(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public static RedisDTO of(String key, Object value) {
        return new RedisDTO(key, value);
    }
}
