package org.example.project.common.middleware.service.redis;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 封装 ValueOperations
 *
 * @author wenxy
 * @date 2020/10/9
 */
public interface RedisValueService<K, V> {

    /**
     * Set {@code value} for {@code key}.
     *
     * @param key   key
     * @param value value
     */
    void set(K key, V value);

    /**
     * Set the {@code value} and expiration {@code timeout} for {@code key}.
     *
     * @param key     key
     * @param value   value
     * @param timeout timeout
     */
    void set(K key, V value, Duration timeout);

    /**
     * Set {@code key} to hold the string {@code value} if {@code key} is absent.
     *
     * @param key   key
     * @param value value
     * @return
     */
    Boolean setIfAbsent(K key, V value);

    /**
     * Set {@code key} to hold the string {@code value} and expiration {@code timeout} if {@code key} is absent.
     *
     * @param key     key
     * @param value   value
     * @param timeout timeout
     * @return
     */
    Boolean setIfAbsent(K key, V value, Duration timeout);

    /**
     * Set {@code key} to hold the string {@code value} if {@code key} is present.
     *
     * @param key   key
     * @param value value
     * @return
     */
    Boolean setIfPresent(K key, V value);

    /**
     * Set {@code key} to hold the string {@code value} and expiration {@code timeout} if {@code key} is present.
     *
     * @param key     key
     * @param value   value
     * @param timeout timeout
     * @return
     */
    Boolean setIfPresent(K key, V value, Duration timeout);


    /**
     * Set multiple keys to multiple values using key-value pairs provided in {@code tuple}.
     *
     * @param map multiple key-value pairs
     */
    void multiSet(Map<? extends K, ? extends V> map);

    /**
     * Set multiple keys to multiple values using key-value pairs provided in {@code tuple} only if the provided key doesnot exist.
     *
     * @param map multiple key-value pairs
     * @return
     */
    Boolean multiSetIfAbsent(Map<? extends K, ? extends V> map);

    /**
     * Get the value of {@code key}.
     *
     * @param key key
     * @return
     */
    V get(Object key);

    /**
     * Get multiple {@code keys}. Values are returned in the order of the requested keys.
     *
     * @param keys keys
     * @return
     */
    List<V> multiGet(Collection<K> keys);

    /**
     * Increment an integer value stored as string value under {@code key} by one.
     *
     * @param key key
     * @return
     */
    Long increment(K key);

    /**
     * Increment an integer value stored as string value under {@code key} by {@code delta}.
     *
     * @param key   key
     * @param delta delta
     * @return
     */
    Long increment(K key, long delta);

    /**
     * Decrement an integer value stored as string value under {@code key} by one.
     *
     * @param key key
     * @return
     */
    Long decrement(K key);

    /**
     * Decrement an integer value stored as string value under {@code key} by {@code delta}.
     *
     * @param key   key
     * @param delta delta
     * @return
     */
    Long decrement(K key, long delta);

    /**
     * Get a substring of value of {@code key} between {@code begin} and {@code end}.
     *
     * @param key   key
     * @param start start
     * @param end   end
     * @return
     */
    String get(K key, long start, long end);

    /**
     * Overwrite parts of {@code key} starting at the specified {@code offset} with given {@code value}.
     *
     * @param key    key
     * @param value  value
     * @param offset offset
     */
    void set(K key, V value, long offset);

    /**
     * Get the length of the value stored at {@code key}.
     *
     * @param key key
     * @return
     */
    Long size(K key);

    /**
     * Sets the bit at {@code offset} in value stored at {@code key}.
     *
     * @param key    key
     * @param offset offset
     * @param value  value
     * @return
     */
    Boolean setBit(K key, long offset, boolean value);

    /**
     * Get the bit value at {@code offset} of value at {@code key}.
     *
     * @param key    key
     * @param offset offset
     * @return
     */
    Boolean getBit(K key, long offset);

}
