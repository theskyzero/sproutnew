package org.example.project.common.middleware.spi.keygen;

import lombok.extern.slf4j.Slf4j;
import org.example.project.common.middleware.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @ClassName OrderIdGenerator
 * @Description: 全局唯一订单号生成工具类
 * @Author yinhang
 * @Date 2019/8/7 13:03
 **/
@Component
@Slf4j
@ConditionalOnClass(RedisTemplate.class)
public class IdGenerator {

    /**
     * redis key for idGenerator
     */
    private static final String LONG_KEY = "db:id";
    int bufferSize = 128;

    @Autowired
    RedisService defaultRedisService;
    LinkedBlockingQueue<Long> queue = new LinkedBlockingQueue();

    public long incr() {
        return defaultRedisService.increment(LONG_KEY);
    }

    public long generateId() {
        return incr();
    }
}
