package org.example.project.common.middleware.service.redis;


import org.example.project.common.middleware.controller.vo.RedisNode;

import java.util.Collection;
import java.util.Properties;

/**
 * @author wenxy
 * @date 2020/10/9
 */
public interface RedisClusterService<K, V> {

    /**
     * Ping the given node;
     *
     * @param redisNode@return
     */
    String ping(RedisNode redisNode);

    /**
     * Remove the node from the cluster.
     *
     * @param redisNode
     */
    void forget(RedisNode redisNode);

    /**
     * getSlaves
     *
     * @param redisNode@return
     */
    Collection<String> getSlaves(RedisNode redisNode);

    /**
     * Move slot assignment from one source to target node and copy keys associated with the slot.
     *
     * @param source source
     * @param slot   slot
     * @param target target
     */
    void reShard(RedisNode source, int slot, RedisNode target);

    /**
     * info
     *
     * @return
     */
    Properties info();
}

