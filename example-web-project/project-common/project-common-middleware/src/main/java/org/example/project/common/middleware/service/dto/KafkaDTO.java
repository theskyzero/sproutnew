package org.example.project.common.middleware.service.dto;

import lombok.Data;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author wenxy
 * @date 2020/10/9
 */
@Data
public class KafkaDTO implements Serializable {
    private static final long serialVersionUID = -261734623021475899L;

    @NotBlank
    private String topic;
    private Integer partition;
    private String key;
    private Object value;
    private Long timestamp;

    public ProducerRecord toProduceRecord() {
        return new ProducerRecord(getTopic(), getPartition(), getTimestamp(), getKey(), getValue());
    }
}
