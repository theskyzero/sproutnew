package org.example.project.common.middleware.controller.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wenxy
 * @date 2020/10/21
 */
@Data
public class ElasticsearchVo<T> implements Serializable {
    String index;
    String id;
    T data;
}
