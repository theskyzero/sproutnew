package org.example.project.common.middleware.controller.vo;

import lombok.Data;
import org.springframework.data.redis.connection.RedisClusterNode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * @author wenxy
 * @date 2020/10/9
 */
@Data
public class RedisNode implements Serializable {

    private static final long serialVersionUID = 6579653153608819822L;

    @NotBlank
    String host;
    @Positive
    int port;

    public RedisClusterNode toRedisClusterNode() {
        return new RedisClusterNode(getHost(), getPort());
    }
}
