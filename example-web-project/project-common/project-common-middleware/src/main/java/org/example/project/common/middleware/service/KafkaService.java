package org.example.project.common.middleware.service;

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.TopicPartition;
import org.example.project.common.middleware.service.dto.KafkaDTO;
import org.springframework.kafka.support.SendResult;

import java.util.List;
import java.util.Map;

/**
 * 封装kafka操作
 * <p>
 * kafka operations
 *
 * @author wenxy
 * @date 2020/9/29
 */
public interface KafkaService<K, V> {
    /**
     * Send the data to the provided topic with no key or partition.
     *
     * @param kafkaDTO kafkaDTO
     * @return a Future for the {@link SendResult}.
     */
    String send(KafkaDTO kafkaDTO);

    /**
     * See {@link Producer#partitionsFor(String)}.
     *
     * @param topic the topic.
     * @return the partition info.
     * @since 1.1
     */
    List<String> partitionsFor(String topic);

    /**
     * See {@link Producer#metrics()}.
     *
     * @return the metrics.
     * @since 1.1
     */
    Map<String, Object> metrics();

    /**
     * When running in a transaction, send the consumer offset(s) to the transaction. It
     * is not necessary to call this method if the operations are invoked on a listener
     * container thread (and the listener container is configured with a
     * {@link org.springframework.kafka.transaction.KafkaAwareTransactionManager}) since
     * the container will take care of sending the offsets to the transaction.
     *
     * @param offsets         The offsets.
     * @param consumerGroupId the consumer's group.id.
     * @since 1.3
     */
    void sendOffsetsToTransaction(Map<TopicPartition, OffsetAndMetadata> offsets, String consumerGroupId);
}
