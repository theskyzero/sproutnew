package org.example.project.common.middleware.service.redis;

import java.util.Collection;
import java.util.List;

/**
 * @author wenxy
 * @date 2020/10/9
 */
public interface RedisListService<K, V> {

    /**
     * Get elements between {@code begin} and {@code end} from list at {@code key}.
     *
     * @param key   key
     * @param start start
     * @param end   end
     * @return values
     */
    List<V> lGet(K key, long start, long end);

    /**
     * Trim list at {@code key} to elements between {@code start} and {@code end}.
     *
     * @param key   key
     * @param start start
     * @param end   end
     */
    void lRemove(K key, long start, long end);

    /**
     * Get the size of list stored at {@code key}.
     *
     * @param key key
     * @return size of list
     */
    Long lSize(K key);

    /**
     * Prepend {@code value} to {@code key}.
     *
     * @param key   key
     * @param value value
     * @return
     */
    Long lAddFirst(K key, V value);

    /**
     * Prepend {@code value} to {@code key}.
     *
     * @param key    key
     * @param values values
     * @return
     */
    Long lAddFirst(K key, Collection<V> values);

    /**
     * Prepend {@code values} to {@code key} only if the list exists.
     *
     * @param key   key
     * @param value value
     * @return
     */
    Long lAddFirstIfPresent(K key, V value);


    /**
     * Append {@code value} to {@code key}.
     *
     * @param key   key
     * @param value value
     * @return
     */
    Long lAdd(K key, V value);

    /**
     * Append {@code values} to {@code key}.
     *
     * @param key
     * @param values
     * @return
     */
    Long lAdd(K key, Collection<V> values);

    /**
     * Append {@code values} to {@code key} only if the list exists.
     *
     * @param key
     * @param value
     * @return
     */
    Long lAddIfPresent(K key, V value);

    /**
     * Set the {@code value} list element at {@code index}.
     *
     * @param key   key
     * @param index index
     * @param value value
     */
    void lAdd(K key, long index, V value);

    /**
     * Get element at {@code index} form list at {@code key}.
     *
     * @param key   key
     * @param index index
     * @return
     */
    V lGet(K key, long index);

}
