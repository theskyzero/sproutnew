package org.example.project.common.middleware.service;

import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.query.AliasQuery;

import java.util.List;
import java.util.Map;

/**
 * @author wenxy
 * @date 2020/10/21
 */
public interface ElasticsearchIndexService {
    /**
     * Create an index.
     *
     * @return {@literal true} if the index was created
     */
    boolean createIndex(String index);

    /**
     * Create an index for given Settings.
     *
     * @param settings the index settings
     * @return {@literal true} if the index was created
     */
    boolean createIndex(String index, Document settings);

    /**
     * Deletes the index this  is bound to
     *
     * @return {@literal true} if the index was deleted
     */
    boolean deleteIndex(String index);

    /**
     * Checks if the index this IndexOperations is bound to exists
     *
     * @return {@literal true} if the index exists
     */
    boolean indexExists(String index);

    /**
     * Creates the index mapping for the entity this IndexOperations is bound to.
     *
     * @return mapping object
     */
    Document createMapping(String index);

    /**
     * writes a mapping to the index
     *
     * @param mapping the Document with the mapping definitions
     * @return {@literal true} if the mapping could be stored
     */
    boolean putMapping(String index, Document mapping);

    /**
     * Get mapping for an index defined by a class.
     *
     * @return the mapping
     */
    Map<String, Object> getMapping(String index);

    /**
     * Add an alias.
     *
     * @param query query defining the alias
     * @return true if the alias was created
     */
    boolean addAlias(AliasQuery query);

    /**
     * Get the alias informations for a specified index.
     *
     * @return alias information
     */
    List<AliasMetaData> queryForAlias(String index);

    /**
     * Remove an alias.
     *
     * @param query query defining the alias
     * @return true if the alias was removed
     */
    boolean removeAlias(AliasQuery query);

    /**
     * Get the index settings.
     *
     * @return the settings
     */
    Map<String, Object> getSettings(String index);

    /**
     * Get settings for a given indexName.
     *
     * @param includeDefaults wehther or not to include all the default settings
     * @return the settings
     */
    Map<String, Object> getSettings(String index, boolean includeDefaults);
}
