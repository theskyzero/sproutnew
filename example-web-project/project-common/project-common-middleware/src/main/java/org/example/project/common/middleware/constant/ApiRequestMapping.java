package org.example.project.common.middleware.constant;

/**
 * @author wenxy
 * @date 2020/10/9
 */
public interface ApiRequestMapping {

    String MIDDLEWARE_API = "/api/middleware";

    String REDIS_API = MIDDLEWARE_API + "/redis";

    String KAFKA_API = MIDDLEWARE_API + "/kafka";

    String ELASTICSEARCH_API = MIDDLEWARE_API + "/elasticsearch";

    String DEMO = "/demo";
}
