package org.example.project.common.middleware.service.impl;

import org.example.project.common.middleware.elasticsearch.ElasticsearchRestTemplate;
import org.example.project.common.middleware.elasticsearch.core.IndexCoordinates;
import org.example.project.common.middleware.elasticsearch.core.dto.Document;
import org.example.project.common.middleware.service.ElasticsearchIndexService;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wenxy
 * @date 2020/10/21
 */
@Service
public class ElasticsearchIndexServiceImpl implements ElasticsearchIndexService {

    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * Create an index.
     *
     * @param index
     * @return {@literal true} if the index was created
     */
    @Override
    public boolean createIndex(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).create();
    }

    /**
     * Create an index for given Settings.
     *
     * @param index
     * @param settings the index settings
     * @return {@literal true} if the index was created
     */
    @Override
    public boolean createIndex(String index, Document settings) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).create(settings);
    }

    /**
     * Deletes the index this  is bound to
     *
     * @param index
     * @return {@literal true} if the index was deleted
     */
    @Override
    public boolean deleteIndex(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).delete();
    }

    /**
     * Checks if the index this IndexOperations is bound to exists
     *
     * @param index
     * @return {@literal true} if the index exists
     */
    @Override
    public boolean indexExists(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).exists();
    }

    /**
     * Creates the index mapping for the entity this IndexOperations is bound to.
     *
     * @param index
     * @return mapping object
     */
    @Override
    public Document createMapping(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).createMapping();
    }

    /**
     * writes a mapping to the index
     *
     * @param index
     * @param mapping the Document with the mapping definitions
     * @return {@literal true} if the mapping could be stored
     */
    @Override
    public boolean putMapping(String index, Document mapping) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).putMapping(mapping);
    }

    /**
     * Get mapping for an index defined by a class.
     *
     * @return the mapping
     */
    @Override
    public Map<String, Object> getMapping(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).getMapping();
    }

    /**
     * Add an alias.
     *
     * @param query query defining the alias
     * @return true if the alias was created
     */
    @Override
    public boolean addAlias(AliasQuery query) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(query.getIndexName())).addAlias(query);
    }

    /**
     * Get the alias informations for a specified index.
     *
     * @param index
     * @return alias information
     */
    @Override
    public List<AliasMetaData> queryForAlias(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).queryForAlias();
    }

    /**
     * Remove an alias.
     *
     * @param query query defining the alias
     * @return true if the alias was removed
     */
    @Override
    public boolean removeAlias(AliasQuery query) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(query.getIndexName())).removeAlias(query);
    }

    /**
     * Get the index settings.
     *
     * @param index
     * @return the settings
     */
    @Override
    public Map<String, Object> getSettings(String index) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).getSettings();
    }

    /**
     * Get settings for a given indexName.
     *
     * @param index
     * @param includeDefaults wehther or not to include all the default settings
     * @return the settings
     */
    @Override
    public Map<String, Object> getSettings(String index, boolean includeDefaults) {
        return elasticsearchRestTemplate.indexOps(IndexCoordinates.of(index)).getSettings(includeDefaults);
    }
}
