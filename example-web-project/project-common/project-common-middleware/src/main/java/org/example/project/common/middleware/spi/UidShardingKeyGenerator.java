package org.example.project.common.middleware.spi;

import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import java.util.Properties;

/**
 * TODO 接入订单中心分布式id
 *
 * @author wenxy
 * @date 2020/10/14
 */
public class UidShardingKeyGenerator implements ShardingKeyGenerator {
    private static final String TYPE = "uid";

    Properties properties;

    /**
     * Generate key.
     *
     * @return generated key
     */
    @Override
    public Comparable<?> generateKey() {
        return null;
    }

    /**
     * Get algorithm type.
     *
     * @return type
     */
    @Override
    public String getType() {
        return TYPE;
    }

    /**
     * Get properties.
     *
     * @return properties of algorithm
     */
    @Override
    public Properties getProperties() {
        return properties;
    }

    /**
     * Set properties.
     *
     * @param properties properties of algorithm
     */
    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
