package org.example.project.common.middleware.service.impl;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.example.project.common.middleware.service.ElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchHitsIterator;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.*;

import java.util.List;

/**
 * @author wenxy
 * @date 2020/10/21
 */
@Configuration("defaultElasticsearchService")
@ConditionalOnBean(ElasticsearchRestTemplate.class)
public class DefaultElasticsearchServiceImpl implements ElasticsearchService {

    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Override
    public <T> T save(T entity, String index) {
        return elasticsearchRestTemplate.save(entity, IndexCoordinates.of(index));
    }

    @Override
    public <T> Iterable<T> save(Iterable<T> entities, String index) {
        return elasticsearchRestTemplate.save(entities, IndexCoordinates.of(index));
    }

    @Override
    public String index(IndexQuery query, String index) {
        return elasticsearchRestTemplate.index(query, IndexCoordinates.of(index));
    }

    @Override
    public <T> T get(String id, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.get(id, clazz, IndexCoordinates.of(index));
    }

    @Override
    public <T> List<T> multiGet(Query query, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.multiGet(query, clazz, IndexCoordinates.of(index));
    }

    @Override
    public boolean exists(String id, String index) {
        return elasticsearchRestTemplate.exists(id, IndexCoordinates.of(index));
    }

    @Override
    public List<String> bulkIndex(List<IndexQuery> queries, BulkOptions bulkOptions, String index) {
        return elasticsearchRestTemplate.bulkIndex(queries, bulkOptions, IndexCoordinates.of(index));
    }

    @Override
    public void bulkUpdate(List<UpdateQuery> queries, BulkOptions bulkOptions, String index) {
        elasticsearchRestTemplate.bulkUpdate(queries, bulkOptions, IndexCoordinates.of(index));
    }

    @Override
    public String delete(String id, String index) {
        return elasticsearchRestTemplate.delete(id, IndexCoordinates.of(index));
    }

    @Override
    public String delete(Object entity, String index) {
        return elasticsearchRestTemplate.delete(entity, IndexCoordinates.of(index));
    }

    @Override
    public void delete(Query query, Class<?> clazz, String index) {
        elasticsearchRestTemplate.delete(query, clazz, IndexCoordinates.of(index));
    }

    @Override
    public long count(Query query, Class<?> clazz, String index) {
        return elasticsearchRestTemplate.count(query, clazz, IndexCoordinates.of(index));
    }

    @Override
    public SearchResponse suggest(SuggestBuilder suggestion, String index) {
        return elasticsearchRestTemplate.suggest(suggestion, IndexCoordinates.of(index));
    }

    @Override
    public <T> List<SearchHits<T>> multiSearch(List<? extends Query> queries, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.multiSearch(queries, clazz, IndexCoordinates.of(index));
    }

    @Override
    public List<SearchHits<?>> multiSearch(List<? extends Query> queries, List<Class<?>> classes, String index) {
        return elasticsearchRestTemplate.multiSearch(queries, classes, IndexCoordinates.of(index));
    }

    @Override
    public <T> SearchHits<T> search(Query query, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.search(query, clazz, IndexCoordinates.of(index));
    }

    @Override
    public <T> SearchHits<T> search(MoreLikeThisQuery query, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.search(query, clazz, IndexCoordinates.of(index));
    }

    @Override
    public <T> SearchHitsIterator<T> searchForStream(Query query, Class<T> clazz, String index) {
        return elasticsearchRestTemplate.searchForStream(query, clazz, IndexCoordinates.of(index));
    }

}
